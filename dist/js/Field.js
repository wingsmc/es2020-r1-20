class Field {
    constructor(_nmines, _i) {
        this.i = _i;
        this.nMines = _nmines;
        /**
         * @access private
         * @typedef Boolean
        */
        this._flagged = false;
        /**@private */
        this._turned = false;

        this.dom = document.createElement('div');   // referencia a vele asszociált DOM objektumra
        // E miatt a sor miatt váltottam TypeScript-ről natív JS-re 😑 ahhoz,
        // hogy webpacket használjak meg nem ittam eleget.
        this.dom.fieldRef = this;                   // referencia a vele asszociált Field objektumra.
        this.dom.classList.add('field');
        this.dom.dataset.nmines = this.nMines;
        this.flagged = false;
        this.turned = false;
        this.dom.addEventListener('click', Field.onClick);
        this.dom.addEventListener('contextmenu', Field.onFlag);
        GLOBALS.area.appendChild(this.dom);
    }
    get flagged() { return this._flagged; }
    set flagged(v) { this._flagged = v; this.dom.dataset.flagged = v ? '1' : '0'; }
    get turned() { return this._turned; }
    set turned(v) { this._turned = v; this.dom.dataset.turned = v ? '1' : '0'; }
    /**
     * Elméletileg ez rekurzívan fel fogja tárni az
     * összes 0-s mezőt(ha szomszédok) és azok nem 0-s
     * szomszédjait
     * Egyébként ha kellőképpen sok 0 értékű mező van egymás mellett akkor
     * StaCk oVefLow
     */
    revealNeighbours() {
        let mod = this.i % CONFIG.size;
        if (mod != 0) {                                                                                     // balra -
            if(GLOBALS.fieldArr[this.i-CONFIG.size-1]) GLOBALS.fieldArr[this.i-CONFIG.size-1].dom.click();  // fent
                                                       GLOBALS.fieldArr[this.i-1].dom.click();              // középen
            if(GLOBALS.fieldArr[this.i+CONFIG.size-1]) GLOBALS.fieldArr[this.i+CONFIG.size-1].dom.click();  // lent
        }
        if (mod != CONFIG.size - 1) {                                                                       // jobbra -
            if(GLOBALS.fieldArr[this.i-CONFIG.size+1]) GLOBALS.fieldArr[this.i-CONFIG.size+1].dom.click();  // fent
                                                       GLOBALS.fieldArr[this.i+1].dom.click();              // középen
            if(GLOBALS.fieldArr[this.i+CONFIG.size+1]) GLOBALS.fieldArr[this.i+CONFIG.size+1].dom.click();  // lent
        }
        if(GLOBALS.fieldArr[this.i-CONFIG.size]) GLOBALS.fieldArr[this.i-CONFIG.size].dom.click();          // fölötte
        if(GLOBALS.fieldArr[this.i+CONFIG.size]) GLOBALS.fieldArr[this.i+CONFIG.size].dom.click();          // alatta
    }

    expose() {
        if(this.flagged && this.nMines == 9) return;
        else if(this.flagged) {
            this.dom.style.color = 'rgb(255, 0, 0)';
            this.turned = true;
            this.flagged = false;
        } else {
            this.turned = true;
        }
    }
    /**
     * Ez felfedi az összes mező értékét
     * ⚠ WARNING: Csak az után használni miután a Status.ended=true
     */
    static revealAll() {
        for(const field of GLOBALS.fieldArr) field.expose();
    }
    /**
     * @callback div.field.onclick
     */
    static onClick() {
        // this : a div.field amire rá klikkeltek
        if(this.fieldRef.flagged) return;
        this.removeEventListener('click', Field.onClick);
        this.fieldRef.turned = true;
        if(this.fieldRef.nMines == 9) {
            this.style.borderRadius = '50%';
            if(!Status.ended) Status.lose();
        } else {
            if(this.fieldRef.nMines == 0) {
                this.fieldRef.revealNeighbours();
            } else {
                this.style.color = `hsl(${260 - this.fieldRef.nMines * 27}, 100%, 50%)`;
            }
            if(--GLOBALS.remFields == 0 && !Status.ended) Status.win();
        }
    }
    /**
     * @callback div.field.oncontextmenu
     */
    static onFlag(e) {
        e.preventDefault();
        if(this.fieldRef.turned) return false;
        if(this.fieldRef.flagged) {
            --GLOBALS.remMines;
            this.fieldRef.flagged = false;
        } else {
            ++GLOBALS.remMines;
            this.fieldRef.flagged = true;
        }
    }
}
