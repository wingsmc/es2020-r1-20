class Status {
    static init() {
        // TAKARÍTÁS
        Status.ended = false;
        clearInterval(Status.interval);
        while (GLOBALS.area.firstChild) GLOBALS.area.removeChild(GLOBALS.area.firstChild);
        GLOBALS.area.classList.remove('disabled');
        GLOBALS.alert.classList.add('hidden');
        CONFIG.size = Number(document.getElementById('size').value);
        CONFIG.mines = Number(document.getElementById('mines').value);

        // Beállítjuk a --num css custom property értékét (9 || 16) az aknák számára
        document.documentElement.style.setProperty('--num', CONFIG.size.toString());
        CONFIG.sizePowd = CONFIG.size ** 2;
        GLOBALS.remFields = CONFIG.sizePowd - CONFIG.mines;

        Generator.generateMines();
        Generator.generateFields();

        Status.startTime = Date.now()
        let timer = document.getElementById('timer');
        Status.interval = setInterval(() => {
            timer.innerText = ((Date.now() - Status.startTime) / 1000).toPrecision(3).toString();
        }, 100);
    }

    static lose() {
        GLOBALS.alert.firstElementChild.innerText = 'Legközelebb talán nagyobb szerencséd lesz!';
        Status.end();
        Field.revealAll();
    }
    static win() {
        GLOBALS.alert.firstElementChild.innerText = 'Gratulálunk, győztél!';
        Status.end();
    }

    static end() {
        clearInterval(Status.interval);
        Status.ended = true;
        GLOBALS.area.classList.add('disabled');
        GLOBALS.alert.classList.remove('hidden');
    }
}
