class Generator {
    static generateMines() {
        GLOBALS.numArr = new Array(CONFIG.sizePowd);
        for(let fieldIndex, i = 0; i < CONFIG.mines; ++i) {
            while(true) {
                fieldIndex = Math.floor(CONFIG.sizePowd * Math.random());
                if(GLOBALS.numArr[fieldIndex] != 9) break;
            }
            GLOBALS.numArr[fieldIndex] = 9;
        }
    }
    static generateFields() {
        GLOBALS.fieldArr = new Array(CONFIG.sizePowd);
        for(let i = 0; i < CONFIG.sizePowd; ++i) {
            let nMines = 0;
            if(GLOBALS.numArr[i] == 9) nMines = 9;
            else {
                if(GLOBALS.numArr[i-CONFIG.size] == 9) ++nMines;
                if(GLOBALS.numArr[i+CONFIG.size] == 9) ++nMines;
                let mod = i % CONFIG.size;
                if (mod != 0) {
                    if(GLOBALS.numArr[i-CONFIG.size-1] == 9) ++nMines;
                    if(GLOBALS.numArr[i-1] == 9            ) ++nMines;
                    if(GLOBALS.numArr[i+CONFIG.size-1] == 9) ++nMines;
                }
                if (mod != CONFIG.size - 1) {
                    if(GLOBALS.numArr[i-CONFIG.size+1] == 9) ++nMines;
                    if(GLOBALS.numArr[i+1] == 9            ) ++nMines;
                    if(GLOBALS.numArr[i+CONFIG.size+1] == 9) ++nMines;
                }
            }
            GLOBALS.fieldArr[i] = new Field(nMines, i);
        }
        delete GLOBALS.numArr;
    }
}
