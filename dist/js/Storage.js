const CONFIG = {                            // Ezt kell átállítani a menüben
    mines: 10,                              // aknák száma
    size: 9,                                // Sor/Oszlop méret
    sizePowd: null                          // size^2
}

/**
 * JS-ben a többdimenziós tömb nem pont úgy funkcionál mint
 * azt az ember akarja így mindenre 1D tömböt használtam.
 * +A js szótlanul tűri ha egy 1D tömbből kiindexelünk.
 */
const GLOBALS = {
    area: document.getElementById('area'),      // ez a div.area amibe a div.field-ek mennek
    alert: document.getElementById('alert'),    // ez a div.area amibe a div.field-ek mennek
    numArr: null,                               // ez az akna generálásban vesz részt
    fieldArr: null,                             // ez az a tömb amibe a Field osztály példányai mennek
    remFields: null,                            // ha eléri a 0-t nyertél
    remMines: null                              // akna számláló
}
